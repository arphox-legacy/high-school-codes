﻿namespace PalfysKorszak
{
    using System;
    using System.IO;

    class StartupLogger
    {
        static void Run()
        {
            Console.ForegroundColor = ConsoleColor.White;
            FileStream fs = new FileStream("StartUp.log", FileMode.Append);
            StreamWriter sw = new StreamWriter(fs);
            sw.Write(DateTime.Now);
            string user = Environment.GetEnvironmentVariable("USERNAME");
            sw.WriteLine("\t" + user);
            sw.Close();
            fs.Close();
        }
    }
}