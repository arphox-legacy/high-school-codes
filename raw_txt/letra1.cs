using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace letra1
{
    class Program 
    {
        static int a = 0, b = 0, osszeg = 0, hatarertek = 0;
        static int hatarkezdo = 20, hatarvege = 200; //ezeket ha �t�rod, a program m�s hat�r�rt�kek k�z�tt k�r.
        static bool nyertes = false, ellenorzes = false;
        static string jatekos = "";
        static Random x = new Random();

        static void Main(string[] args) 
        {
            Kezdes();
            Console.WriteLine(jatekos + "\t\t G�p \n");
            do
            {
                Input();
                if (osszeg > hatarertek)
                {
                    Console.WriteLine("\nT�ll�pted a " + hatarertek + "-t, a G�p nyert!");
                    break;
                }
                if (osszeg == hatarertek)
                {
                    nyertes = false; //false akkor, ha az A-j�t�kos nyer.
                    break;
                }
                Console.WriteLine("\t" + osszeg); Console.Write("\t");
                Thread.Sleep(1000);
                Geplepes();
                osszeg = osszeg + b;
                Console.WriteLine("\t" + b);
                Thread.Sleep(400);
                if (osszeg > hatarertek)
                {
                    Console.WriteLine("\nA g�p t�ll�pte a " + hatarertek + "-t, a te nyert�l!");
                    Console.ReadKey();
                    Environment.Exit(-1);
                }
                if (osszeg == hatarertek)
                {
                    nyertes = true; //true akkor, ha a G�p nyer.
                    break;
                }
                Console.WriteLine("\t" + osszeg);
            }
            while (true);

            Nyertes();
        }
        static void Kezdes() 
        {
            Console.Clear();
            Console.WriteLine("�dv�z�llek a L�tra logikai j�t�kban!");
            Console.WriteLine("==============================================\nJ�t�kszab�lyok:\n");
            Console.WriteLine("Ketten j�tsz�k a j�t�kot. A j�t�kos �s a g�p.\nFelv�ltva mondanak egy sz�mot 1-10-ig. A g�p a sz�mokat �sszeadja.");
            Console.WriteLine("Aki hamarabb el�ri a hat�r�rt�ket (25), az nyer.");
            Console.WriteLine("==============================================\n");
            Console.Write("Adja meg a j�t�kos nev�t: ");
            jatekos = Convert.ToString(Console.ReadLine());
            hatarertek = 25;

/*            Console.Write("Adj meg egy hat�r�rt�ket "+hatarkezdo+" �s "+hatarvege+" k�z�tt: ");
            do
            {
                try
                {
                    hatarertek = Convert.ToInt32(Console.ReadLine());
                    if (hatarertek < hatarkezdo || hatarertek > hatarvege)
                    {
                        Console.Write("Rossz a megadott sz�m, adj meg egy m�sikat: ");
                    }
                }
                catch (Exception)
                {
                    Console.Write("Rossz a megadott sz�m, adj meg egy m�sikat: ");
                }
            }
            while (hatarertek < hatarkezdo || hatarertek > hatarvege);
 */           Console.Clear();
        }
        static void Input() 
        {
            try
            {
                a = Convert.ToInt32(Console.ReadLine());
                while (a < 1 || a > 10)
                {
                    Console.WriteLine("Csak 1 �s 10 k�z�tti sz�mot �rhatsz be!");
                    a = Convert.ToInt32(Console.ReadLine());
                }
            }
            catch (Exception)
            {
                do
                {
                    Console.WriteLine("Csak 1 �s 10 k�z�tti sz�mot �rhatsz be!");
                    try
                    {
                        a = Convert.ToInt32(Console.ReadLine());
                        ellenorzes = true;
                    }
                    catch (Exception)
                    {
                        ellenorzes = false;
                    }
                }
                while (a < 1 || a > 10 && ellenorzes == true);
            }
            osszeg = osszeg + a;
        }
        static void Geplepes() 
        {
            int h = 25;
            switch (osszeg)
            {
                case 1: b = 2; break;
                case 2: b = 1; break;
                case 3: b = x.Next(1, 10); break;
                case 4: b = 10; break;
                case 5: b = 9; break;
                case 6: b = 8; break;
                case 7: b = 7; break;
                case 8: b = 6; break;
                case 9: b = 5; break;
                case 10: b = 4; break;
                case 11: b = 3; break;
                case 12: b = 2; break;
                case 13: b = 1; break;
                case 14: b = x.Next(1, 10); break;
                case 15: b = h - 15; break;
                case 16: b = h - 16; break;
                case 17: b = h - 17; break;
                case 18: b = h - 18; break;
                case 19: b = h - 19; break;
                case 20: b = h - 20; break;
                case 21: b = h - 21; break;
                case 22: b = h - 22; break;
                case 23: b = h - 23; break;
                case 24: b = h - 24; break;
            }
        }
        static void Nyertes() 
        {
            if (nyertes == false)
            {
                Console.WriteLine("\t" + hatarertek);
                Console.WriteLine("\n" + jatekos + " nyert!");
            }
            else //nyertes csak true lehet
            {
                Console.WriteLine("\t" + hatarertek);
                Console.WriteLine("\nA G�p nyert!");
            }
            Console.ReadKey();
        }
    }
}
