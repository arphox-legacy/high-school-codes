using System;
using System.Numerics;
using System.Windows.Forms;

class Program
{
    static void Main()
    {
        MessageBox.Show("Kedves felhaszn�l�!\n\nEz egy replusz m�velet elv�gz�, amely komplex sz�mokkal is tud sz�molni. Haszn�lata egyszer�: be�rod enterrel elv�lasztva a k�t komplex sz�mot, majd ki�rja az eredm�nyt.\nElvileg egyetlen hi�nyoss�ga van: ha a k�pzetes r�sz 1, akkor az 1-et sz�ks�ges el��rni, teh�t a \"j\" nem m�k�dik, de az \"1j\" igen!\nHa hib�t �szlel a program, akkor �jrak�ri a sz�mokat.\n\nKellemes villanytanoz�st! :)");

        Console.ForegroundColor = ConsoleColor.White;
        Console.Title = "Komplex replusz sz�mol� by. Ozsv�rt K�roly";
        while (true)
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Add meg a k�t sz�mot enterrel elv�lasztva! (a+bj ENTER c+dj)");
                string be1 = Console.ReadLine();
                string be2 = Console.ReadLine();
                string[] egyik = be1.Split('+', '-');
                string[] masik = be2.Split('+', '-');
                Complex a, b;
                if (egyik.Length == 2) //ha van komplex r�sze
                    a = new Complex(Convert.ToDouble(egyik[0]), Convert.ToDouble(egyik[1].Replace("j", "")));
                else if (egyik[0].Contains("j"))
                    a = new Complex(0, Convert.ToDouble(egyik[0].Replace("j", "")));
                else
                    a = new Complex(Convert.ToDouble(egyik[0]), 0);

                if (masik.Length == 2) //ha van komplex r�sze
                    b = new Complex(Convert.ToDouble(masik[0]), Convert.ToDouble(masik[1].Replace("j", "")));
                else if (masik[0].Contains("j"))
                    b = new Complex(0, Convert.ToDouble(masik[0].Replace("j", "")));
                else
                    b = new Complex(Convert.ToDouble(masik[0]), 0);

                Complex eredmeny = Complex.Multiply(a, b) / Complex.Add(a, b);
                Console.WriteLine();
                if (eredmeny.Imaginary < 0)
                    Console.WriteLine("{0} - {1} j", eredmeny.Real, eredmeny.Imaginary);
                else if (eredmeny.Imaginary > 0)
                    Console.WriteLine("{0} + {1} j", eredmeny.Real, eredmeny.Imaginary);
                else
                    Console.WriteLine("{0}", eredmeny.Real);
                Console.ReadLine();
            }
            catch { }
        }
    }
}